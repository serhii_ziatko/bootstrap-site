const gulp = require('gulp');
const concat = require('gulp-concat');
const sourcemap = require('gulp-sourcemaps');
const minifyCss = require('gulp-cssnano');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const sequence = require('gulp-sequence');
const browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');


gulp.task('html', () => {
  gulp.src('src/*.html')
    .pipe(gulp.dest('public'));
});

gulp.task('style', () => {
  gulp.src([ 'node_modules/bootstrap/dist/css/bootstrap.css' , 'node_modules/bootstrap/dist/css/bootstrap-theme.css'  , 'src/css/**/*.css'])
   .pipe(sourcemap.init())
   .pipe(autoprefixer({ browsers: 'last 5 versions' }))
   .pipe(concat('main.css'))
   .pipe(minifyCss())
   .pipe(sourcemap.write())
   .pipe(gulp.dest('public/css'));
});

gulp.task('images', () => {
  gulp.src('src/img/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('public/img'));
});

gulp.task('fonts', () => {
  gulp.src('src/fonts/**/*.*') 
    .pipe(gulp.dest('public/fonts'));
});

gulp.task('clean', () => {
  return del(['public']); 
});

gulp.task('lint', () => {
  gulp.src('src/js/*.js')
    .pipe(eslint())
    .pipe(eslint.format());
});


gulp.task('serve', () => {
  browserSync.init({
    server: 'public'
  });
  browserSync.watch('public/**/*.*').on('change', browserSync.reload);
});

gulp.task('js', () => {
  gulp.src([ 'node_modules/jquery/dist/jquery.js', 'node_modules/bootstrap/dist/js/bootstrap.js' ,'src/js/*.js'])
    .pipe(sourcemap.init())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(sourcemap.write())
    .pipe(gulp.dest('public/js'));
});

gulp.task('watch', () => {
  gulp.watch('src/*.html', ['html']);
  gulp.watch('src/css/*.css', ['style']);
  gulp.watch('src/js/*.js', ['js']);
});

gulp.task('build', sequence('clean',['style', 'html', 'js', 'images', 'fonts'], 'watch', 'serve'));